var rad30 = 30*Math.PI/180;
var sen30 = Math.sin(rad30);
var cos30 = Math.cos(rad30);
var ancho = 200*cos30;
var alto = 200;

class Cubo {
    constructor(context, x, y) {
        this.context = context;
        this.x = x;
        this.y = y;
    }

    pintar() {
        this.context.beginPath(); 
        this.context.moveTo(this.x, this.y+(alto/4));
        this.context.lineTo(this.x, this.y+(alto/4)*3);
        this.context.lineTo(this.x+(ancho/2), this.y+alto);
        this.context.lineTo(this.x+(ancho), this.y+((alto/4)*3));
        this.context.lineTo(this.x+ancho, this.y+(alto/4));
        this.context.lineTo(this.x+(ancho/2), this.y);
        this.context.lineTo(this.x, this.y+(alto/4));
        this.context.lineTo(this.x+(ancho/2), this.y+(alto/2));
        this.context.lineTo(this.x+(ancho/2), this.y+alto);
        this.context.moveTo(this.x+(ancho/2), this.y+(alto/2));
        this.context.lineTo(this.x+ancho, this.y+(alto/4));
        this.context.stroke();
    }

    desplazarNE(desplazamiento) {
        this.x += (ancho/2) * desplazamiento;
        this.y -= (alto/4) * desplazamiento;
    }

    desplazarNO(desplazamiento) {
        this.x -= (ancho/2) * desplazamiento;
        this.y -= (alto/4) * desplazamiento;
    }

    desplazarSE(desplazamiento) {
        this.x += (ancho/2) * desplazamiento;
        this.y += (alto/4) * desplazamiento;
    }

    desplazarSO(desplazamiento) {
        this.x -= (ancho/2) * desplazamiento;
        this.y += (alto/4) * desplazamiento;
    }

    elevarZ(desplazamiento) {
        this.y -= (alto/2) * desplazamiento;
    }

    bajarZ(desplazamiento) {
        this.y += (alto/2) * desplazamiento;
    }
}

class StaticItem {
    constructor(context, img, x, y, tamX, tamY) {
        this.context = context;
        this.img = img;
        this.x = x;
        this.y = y;
        this.tamX = tamX;
        this.tamY = tamY;
    }

    pintar() {
        this.context.drawImage(this.img, this.x, this.y, this.tamX, this.tamY);
    }
}

class Item {
    constructor(context, img, x, y, tamX, tamY) {
        this.context = context;
        this.img = img;
        this.x = x;
        this.y = y;
        this.tamX = tamX;
        this.tamY = tamY;
    }

    pintar() {
        this.context.drawImage(this.img, this.x, this.y, this.tamX, this.tamY);
    }

    desplazarNE(desplazamiento) {
        this.x += (ancho/2) * desplazamiento;
        this.y -= (alto/4) * desplazamiento;
    }

    desplazarNO(desplazamiento) {
        this.x -= (ancho/2) * desplazamiento;
        this.y -= (alto/4) * desplazamiento;
    }

    desplazarSE(desplazamiento) {
        this.x += (ancho/2) * desplazamiento;
        this.y += (alto/4) * desplazamiento;
    }

    desplazarSO(desplazamiento) {
        this.x -= (ancho/2) * desplazamiento;
        this.y += (alto/4) * desplazamiento;
    }

    elevarZ(desplazamiento) {
        this.y -= (alto/2) * desplazamiento;
    }

    bajarZ(desplazamiento) {
        this.y += (alto/2) * desplazamiento;
    }
}

function main() {
    // console.log("hola");
      
    var canvas = document.getElementById('board_canvas');
    var context = canvas.getContext('2d');

    //Rectángulo
    // context.beginPath(); 
    // context.moveTo(0,0);
    // context.lineTo(0,alto);
    // context.lineTo(ancho,alto);
    // context.lineTo(ancho,0);
    // context.lineTo(0,0);
    // context.stroke();

    // pintarCubo(context, 0, 0);
    var img = document.getElementById("pc_color");

    //var c11 = new Item(context, img, ancho, alto);
    //c11.pintar();
    //var c11 = new Cubo(context, ancho, alto);
    //c11.pintar();
    // c11.desplazarNE(1);
    // c11.pintar();
    // pintarCubo(context, (ancho/2), (alto/4));

    var background = document.getElementById("room1");
    var shelf = document.getElementById("shelf");

    var backgroundItem = new StaticItem(context, background, 0, 0, ancho*7, alto*6);
    backgroundItem.pintar();

    var shelfItem = new Item(context, shelf, 0, 0, ancho*2, alto*2.5)
    shelfItem.pintar();


   document.addEventListener('keyup', (e) => {
        console.log(e.code);
        if (e.code === "ArrowUp") {
            shelfItem.desplazarNE(1);
        } else if (e.code === "ArrowDown") {
            shelfItem.desplazarSO(1);
        } else if (e.code === "ArrowLeft") {
            shelfItem.desplazarNO(1);
        } else if (e.code === "ArrowRight") {
            shelfItem.desplazarSE(1);
        }
        context.clearRect(0,0,canvas.width,canvas.height)
        backgroundItem.pintar();
        shelfItem.pintar();
    });
}
